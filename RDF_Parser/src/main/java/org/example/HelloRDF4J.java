package org.example;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.DC;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class HelloRDF4J {

	public static void main(String[] args) throws IOException {
		JSONParser parser = new JSONParser();

		// Ontology namespaces
		String auth_csd = "http://www.semanticweb.org/auth_csd#";
		String fabio = "http://purl.org/spar/fabio/";

		// Init Model
		ModelBuilder builder = new ModelBuilder();
		ValueFactory f = SimpleValueFactory.getInstance();
		
		IRI conferencePr = f.createIRI(auth_csd, "ConferencePr");
		IRI conferenceAn = f.createIRI(auth_csd, "ConferenceAn");
		IRI magazineArticle = f.createIRI(fabio, "MagazineArticle");
		IRI book = f.createIRI(fabio, "Book");

		try (Reader reader = new FileReader("C:\\Users\\apost\\Documents\\GitHub\\auth_csd\\Dataset\\dataset.json")) {

			// Read all dataset
			JSONObject dataset = (JSONObject) parser.parse(reader);

			// Read collection json object
			JSONObject collection = (JSONObject) dataset.get("collection");

			// Read dc:dc json array
			JSONArray dc = (JSONArray) collection.get("dc:dc");

			Iterator<JSONObject> iterator = dc.iterator();

			int i = 0;
			while (iterator.hasNext()) {
				JSONObject publication = iterator.next();

				// Read dc:identifier json array
				JSONArray identifier = (JSONArray) publication.get("dc:identifier");

				// Declare publication IRI variable
				IRI pub_object;

				Iterator<String> it_identifier = identifier.iterator();
				while (it_identifier.hasNext()) {
					String id = it_identifier.next();
					if (id.contains("CONF-")) {
						// System.out.println("Conference Proceeding" + " : " + id);
						pub_object = f.createIRI(auth_csd, id);
						builder.subject(pub_object);
						builder.add(RDF.TYPE, conferencePr);
					} else if (id.contains("CONFAN")) {
						// System.out.println("Conference Announcement" + " : " + id);
						pub_object = f.createIRI(auth_csd, id);
						builder.subject(pub_object);
						builder.add(RDF.TYPE, conferenceAn);
					} else if (id.contains("ART") || id.contains("XREF")) {
						// System.out.println("Article" + " : " + id);
						pub_object = f.createIRI(auth_csd, id);
						builder.subject(pub_object);
						builder.add(RDF.TYPE, magazineArticle);
					} else if (id.contains("RefW") || id.contains("BOOK") || id.contains("BOOKCH")){
						// In case of RefW, BOOK, BOOKCH
						// System.out.println("Book" + " : " + id);
						pub_object = f.createIRI(auth_csd, id);
						builder.subject(pub_object);
						builder.add(RDF.TYPE, book);
					}
				}

				// Print dc:title
				String title = (String) publication.get("dc:title");
				// System.out.println(i + " : " + title);
				builder.add(DC.TITLE, title);

				// Check if publication has 1 creator or more than 1.
				String type = publication.get("dc:creator").getClass().toString();

				if (type.contains("JSONArray")) {
					// Case of more than 1
					// Read dc:creator json array
					JSONArray creator = (JSONArray) publication.get("dc:creator");

					Iterator<String> it_creator = creator.iterator();
					while (it_creator.hasNext()) {
						String cr = it_creator.next();
						// System.out.println(cr);
						builder.add(DC.CREATOR, cr);
					}
				} else if (type.contains("String")) {
					// Case of 1
					// Read dc:creator string
					String creator = (String) publication.get("dc:creator");
					// System.out.println(creator);
					builder.add(DC.CREATOR, creator);
				}

				i += 1;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// return the Model object
		Model m = builder.build();
		m.setNamespace(RDF.NS);
		m.setNamespace(DC.NS);
		m.setNamespace("fabio", "http://purl.org/spar/fabio/");
		m.setNamespace("auth", auth_csd);

		FileOutputStream out = new FileOutputStream(
				"C:\\Users\\apost\\Documents\\GitHub\\auth_csd\\Dataset\\dataset.ttl");
		try {
			Rio.write(m, out, RDFFormat.TURTLE);
		} finally {
			out.close();
		}

	}
}
